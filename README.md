# Player Hardware

| Reader # | Test HW     | Atmel Firmware | Battery Chip  | HW Patches | Software | Pi Zero Hostname | Pi Zero MAC        | comment |
| ---      | ---         | ---            | ---           |---         | ---      | ---              | ---                | ---     |
| 02       |             | 2021-11-21     | working       | e,p,c      |          | player02         | e4:5f:01:5b:8      ||
| 03       | IR, A       | 2021-11-21     | working       | e,p,c      | F        | player03         | e4:5f:01:5b:8      ||
| 04       | IR, A       | 2021-11-21     | working       | e,p,c      | F        | player04         | e4:5f:01:5b:8b:a9  ||
| 05       |             | 2021-11-21     | working       | e,p,c      |          | player05         | e4:5f:01:5b:8      ||
| 06       | IR, A, 12%  | 2021-11-21     | working       | e,p,c      | F        | player06         | e4:5f:01:5b:8      ||
| 07       | IR, A,  4%  | 2021-11-21     | working       | e,p,c      | F        | player07         | e4:5f:01:5b:8      ||
| 11       | IR, A, 16%  | 2021-11-21     | working       | e,p,c      | F        | player11         | e4:5f:01:5b:8      ||
| 12       |             | 2021-11-21     | working       | e,p,c      | F        | player12         | e4:5f:01:5b:8c:4a  ||
| 14       |             | 2021-11-21     | working       | e,p,c      |          | player14         | e4:5f:01:5b:8      ||
| 15       | IR, A       | 2021-11-21     | working       | e,p,c      | F        | player15         | e4:5f:01:5b:8c:5e  ||
| 16       | IR, A, 20%  | 2021-11-21     | working       | e,p,c      | F        | player16         | e4:5f:01:5b:8      ||
| 21       | IR, A, 16%  | 2021-11-21     | working       | e,p,c      | F        | player21         | e4:5f:01:5b:8      ||
| 22       |             | 2021-11-21     | working       | e,p,c      |          | player22         | e4:5f:01:5b:8      ||
| 24       | IR, A, 22%  | 2021-11-21     | working       | e,p,c      | F        | player24         | e4:5f:01:5b:8      ||
| 27       | IR, A,  4%  | 2021-11-21     | working       | e,p,c      | F        | player27         | e4:5f:01:5b:8      ||
| 31       | IR, A       | 2021-11-21     | working       | e,p,       | F        | player31         | e4:5f:01:5b:8d:46  ||


## List Tests passed
```
IR = IR Sensor triggers Hörzonenwechsel
% = charge after 6 hours playing audio (starting at 97-100%)
A = is playing audio
```

## Software
```
F = has current assets and audioplayer software (29.06.22)
```

## List of HW Patches
```
e = eeprom writeprotect resistor R2 removed
p = power pullup R13 removed
c = added 0 ohm resistor at R9 connecting µC and connector
```
