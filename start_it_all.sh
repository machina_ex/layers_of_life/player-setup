#!/bin/bash

sudo modprobe i2c-dev

cd "$(dirname "$0")"

screen -dmS python_script
screen -S python_script -X stuff "python3 player-firmware/lol_player.py ^M"

screen -dmS player_script
screen -S player_script -X stuff "cd player/ && npm start ^M"
