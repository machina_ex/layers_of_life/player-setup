#!/bin/bash
sudo ln -s /home/pi/player-setup/*.service /etc/systemd/system/
sudo systemctl disable update_machina.service
sudo systemctl enable players_of_life.service

git submodule update --init --recursive

sudo apt install -y python3-smbus python3-pip
sudo pip3 install websockets

sudo systemctl start players_of_life.service
